I hereby issue the following declaration on behalf of the nation of Astrea.

    1. The nation of Astrea is formally established, effective 30 January 2024
       at 14:34 UTC.

    2. I, Jean-Luc Demièn Caliastre, am the Emperor of Astrea and the owner of
       the Imperial Estate of Astrea. This shall remain true regardless of what
       aliases I may use in the future.

    3. I, the Emperor of Astrea, reserve the right to abdicate from the Imperial
       Throne and transfer ownership of the Imperial Estate at my own
       discretion.

    4. I, the Emperor of Astrea, reserve the right to publish a Constitution,
       which shall become effective as the supreme law of Astrea at a date and
       time of my choosing.

    5. Every individual living person or system with one physical human body is
       entitled to apply to the Emperor for Astrean citizenship between the
       period of 30 January 2024, 14:34 UTC to 13 February 2024, 17:30 UTC. This
       does not prevent such persons or systems from applying at later times in
       accordance with the Constitution, once it is published and effective.

    6. I, the Emperor of Astrea, reserve the right to approve or deny requests
       for citizenship at my own discretion, and may require applicants to
       complete an examination within a prescribed deadline.

This declaration is legally binding and enforceable by
Imperial authority, until superseded by the Constitution.
